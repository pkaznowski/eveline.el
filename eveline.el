;; ------------ eveline.el ------------ ;;
;;
;; Eveline is simple implementation of lightline.vim ( https://github.com/itchyny/lightline.vim )
;; and thus designed to be used with `evil-mode'.
;;
;; Current version is not modally customizable yet. Supported blocks are:
;; Left pane:
;; - window number
;; - mode indicator
;; - special buffer indicators (read only, narrowed)
;; - vc branch
;; - buffer name
;; - buffer modified indicator
;; - which function
;; Right pane:
;; - Org-clock indicator ("*")
;; - Python virtual environment name
;; - major mode
;; - buffer position (%)
;; - line: column
;;
;; Eveline uses `mode-line' and `mode-line-inactive' faces adding some
;; new faces allowing modeline colors modification:
;; - `eveline-block-face' for left/right blocks in modeline
;; - `evil-mode' states faces: `eveline-emacs-face',
;;   `eveline-insert-face', `eveline-normal-face' and
;;   `eveline-other-face' which covers visual states.
;; - `eveline-special-edit-face' is used to indicate insert mode in
;;   special editing environments like `occur-edit-mode' or `wdired-mode'
;;
;;
;; Code starts here:
;;
;;* REQUIREMENTS
(require 'dash)
(require 'org-clock)
(require 'switch-window)
(require 'vc-git)

;;* DECLARATIONS
(declare-function mc/num-cursors "multiple-cursors-core" ())

;;* VARIABLES
(defvar modeline-selected-window nil "Stores focused window")
(add-function
 :before pre-redisplay-function
 (lambda (_wins) (setq modeline-selected-window (selected-window))))

(defvar eveline--window-indicators
  '((0 . "➊") (1 . "➋") (2 . "➌")
    (3 . "➍") (4 . "➎") (5 . "➏")
    (6 . "➐") (7 . "➑") (8 . "➒")
    (9 . "➓"))
  "Eveline window number indicator")

;;* FACES
;; NOTE: let's use nord-theme colors for now...
(defface eveline-additional-info-face '((t :foreground "#81a1c1"))
  "Eveline face for info displayed in the center block, like `which-function' string")
(defface eveline-block-face '((t :background "#3b4252"))
  "Eveline face for left/right blocks")
(defface eveline-emacs-face '((t :background "#A3BE8C" :foreground "#3b4252"))
  "Eveline face for EMACS mode")
(defface eveline-insert-face '((t :inverse-video t))
  "Eveline face for INSERT mode")
(defface eveline-normal-face '((t :background "#8acfeb" :foreground "#3b4252"))
  "Eveline face for NORMAL mode")
(defface eveline-other-face '((t :background "#8fbcbb" :foreground "#3b4252"))
  "Eveline face for OTHER (i.e. not EMACS, INSERT nor NORMAL) mode")
(defface eveline-special-edit-face '((t :background "#BF616A" :foreground "#eceff4"))
  "Eveline face indicating insert mode is in special edit buffer
(e.g. `occur-edit-mode' or `wdired-mode')")

;;* FUNCTIONS
(defun eveline--window-has-focus-p ()
  "Return true if buffer has focus."
  (string= (buffer-name) (buffer-name (window-buffer modeline-selected-window))))

(defun eveline--simple-mode-line-render (left right)
  "Return a string of `window-total-width' length containing LEFT and RIGHT aligned respectively."
  (let* ((available-width (- (window-total-width) (length left))))
    (format (format "%%s %%%ds" available-width) left right)))

(defun eveline--pyvenv-name ()
  "Return Python virtual environment name using `pyvenv-virtual-env-name'."
  (when (and
         (member major-mode (list 'python-mode 'ein:ipynb-mode))
         (boundp 'pyvenv-virtual-env-name))
    pyvenv-virtual-env-name))

(defun eveline--shorten-path (path)
  "Replace $HOME with '~'."
  (when path (replace-regexp-in-string (getenv "HOME") "~" path)))

(defun eveline--get-evil-state ()
  "Establish which visual state is on using `evil-visual-tag'."
  (let ((prestate (if evil-mode (upcase (symbol-name evil-state)) "EMACS")))
    (if (string= "VISUAL" prestate)
        (let ((tag (substring (string-trim (evil-visual-tag)) 1 -1)))
          (cond ((string= "V" tag) prestate)
                ((string= "Vb" tag) "V-BLOCK")
                ((string= "Vl" tag) "V-LINE")
                ((string= "Vs" tag) "S-LINE")))
      prestate)))

(defun eveline--get-special-buffer-indicators ()
  "Return concatenated string of special indicators separated with
  '|' where RO means buffer is read only, N - buffer is narrowed."
  (mapconcat
   'identity
   (-filter
    'identity
    (mapcar
     (lambda (p) (when (car p) (cdr p)))
     (-zip `(,buffer-read-only ,(buffer-narrowed-p))
           '(" RO " " N "))))
   "|"))

(defun eveline--get-git-branch ()
  (if-let ((branches (and (buffer-file-name) (car (vc-git-branches)))))
      (format " %s |" branches) ""))

(defun eveline--find-face (base edit)
  (if (member major-mode '(occur-edit-mode wdired-mode)) edit base))

(defun eveline--propertize-state-string (&optional s)
  (let ((state (eveline--get-evil-state)))
    (propertize
     (format " %s " (or s state))
     'face (if (eveline--window-has-focus-p)
               (cond
                ((string= state "EMACS")
                 (eveline--find-face 'eveline-emacs-face
                                     'eveline-special-edit-face))
                ((string= state "INSERT")
                 (eveline--find-face 'eveline-insert-face
                                     'eveline-special-edit-face))
                ((string= state "NORMAL") 'eveline-normal-face)
                (t 'eveline-other-face))
             'mode-line-inactive))))

;;* MODE LINE FORMAT
(setq-default
 mode-line-format
 '((:eval
    (eveline--simple-mode-line-render
     ;; left
     (let ((winnum (assoc-default
                    (cl-position (get-buffer-window) (switch-window--list))
                    eveline--window-indicators))
           (modified (if (and (buffer-modified-p) (not (string-match "*" (buffer-name))))
                         "[+]" "")))
       (if (eveline--window-has-focus-p)
           ;; when window active:
           (let ((special (eveline--get-special-buffer-indicators)))
             (concat
              (propertize (format " %s " winnum) 'face 'eveline-block-face)
              (eveline--propertize-state-string)
              (unless (string= "" special)
                (propertize (format "%s|" special) 'face 'eveline-block-face))
              (propertize (eveline--get-git-branch) 'face 'eveline-block-face)
              (propertize (format " %s " (buffer-name)) 'face 'eveline-block-face)
              modified
              (when (bound-and-true-p multiple-cursors-mode)
                (format "[%d]" (mc/num-cursors)))
              (when-let ((which (when which-function-mode (which-function))))
                (propertize (format " %s " which)
                            'face 'eveline-additional-info-face))))
         ;; when window inactive:
         (format " %s  %s %s"
                 winnum
                 (or (eveline--shorten-path (buffer-file-name)) (buffer-name))
                 modified)))

     ;; right
     (concat
      (when (and (eveline--window-has-focus-p) (org-clocking-buffer)) "* ")
      (when-let ((pyvenv (eveline--pyvenv-name))) (format "%s | " pyvenv))
      (format-mode-line "%m ")
      (propertize
       (format " %3d%%%% "
               (let ((pos (format-mode-line "%p")))
                 (cond ((string= pos "Top") 0)
                       ((member pos '("Bottom" "All")) 100)
                       (t (string-to-number pos)))))
       'face (if (eveline--window-has-focus-p) 'eveline-block-face 'mode-line-inactive))
      (eveline--propertize-state-string (format " %6s  " (format-mode-line "%l:%c"))))))))


(provide 'eveline)

;; eveline.el ends here
